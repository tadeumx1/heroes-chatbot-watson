import styled from 'styled-components';

export const Container = styled.div`

    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;
    padding-top: 100px;

`;

export const LoadImage = styled.img`

    justify-self: center;
    height: 100px;
    width: 100px;

`;

export const ChatbotContainer = styled.div`

    display: flex;
    flex-direction: column;
    background-color: #2C4241;
    width: 100%;
    /* max-width: 400px; */
    height: 500px;
    max-height: 500px;
    border-radius: 20px;
    max-width: 590px;
    box-shadow: 0 10px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    overflow: auto;
    padding-top: 10px;

`;

export const InputButtonContainer = styled.div`

    flex: 1;
    flex-direction: row;
    align-items: center;
    justify-items: center;

`;

export const InformationModal = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    padding-top: 30px;

    input {

        background-color: #FFFFFF;
        border-radius: 10px;
        height: 44px;
        margin-bottom: 10px;
        box-shadow: 0 7px 6px -6px black;
        padding: 0px 9px;
        width: 300px;
        max-width: 300px;

    }

    button {

        background-color: #7A91CA;
        border-radius: 3px;
        height: 44px;
        padding: 0px 20px;
        margin-top: 10px;
        margin-left: 10px;
        border-radius: 10px;
        max-width: 250px;
        align-self: center;
        justify-content: center;
        align-items: center;

    }

    span {

        color: #FFFFFF;
        font-weight: bold;
        font-size: 14px;

    }

`;

export const ListHeroesModal = styled.div`
    padding-top: 10px;
    padding-bottom: 10px;
`;

export const BubbleAuthor = styled.div`

    padding: 6px;
    margin-left: 10px;
    margin-right: 10px;
    background-color: #F5F5F5;
    border-radius: 6px;
    shadow-color: 'rgba(0, 0, 0, 0.5)';
    shadow-offset: 0 1px;
    shadow-opacity: 0.5px;
    shadow-radius: 0px;
    margin-top: 10px;
    margin-bottom: 10px;
    align-self: flex-end;
    background-color: #D1EDC1;
    /* align-self: flex-start; */
    /* maxWidth: width - 60px; */

`;

export const Bubble = styled.div`

    padding: 6px;
    margin-left: 10px;
    margin-right: 10px;
    background-color: #F5F5F5;
    border-radius: 6px;
    shadow-color: 'rgba(0, 0, 0, 0.5)';
    shadow-offset: 0 1px;
    shadow-opacity: 0.5px;
    shadow-radius: 0px;
    margin-top: 10px;
    margin-bottom: 10px;
    align-self: flex-start;
    /* maxWidth: width - 60px; */
    
`;

export const TextAuthor = styled.p`
    font-weight: bold;
    margin-bottom: 3px;
    color: #333;
`;

export const TextMessage = styled.p`
    font-size: 16;
    color: #333;
`;

export const FormContainer = styled.div`
    flex: 1;
    display: flex;
    padding-top: 90px;
    align-items: flex-end;
    width: 100%;
    align-self: flex-end;
`;

export const Form = styled.form`

    width: 100%;
    /* max-width: 400px; */
    display: flex;
    align-items: center;
    justify-content: center;

    input {

        flex: 1;
        height: 55px;
        padding: 0 20px;
        background: #FFF;
        font-size: 18px;
        color: #444;
        border-radius: 3px;

        border: ${props => (props.withError ? '2px solid #F00' : 0)}

    }

    button {

        width: 100px;
        height: 55px;
        padding: 0 20px;
        background: #63F5B8;
        color: #FFF;
        border: 0;
        font-size: 20px;
        font-weight: bold;
        border-radius: 1px;

        /* Usando o SASS quando estamos dentro do componente
         podemos referenciar ele usando o & como por exemplo
         para colocar o hover no botão */

        &:hover {

            background: #52D89F;

        }

    }

    /* Não precisamos sair do componente 
    e escrever button:hover para fazer isso */

`;