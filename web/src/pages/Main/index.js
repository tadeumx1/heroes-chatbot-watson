import React, { Component } from 'react';

import api from '../../services/api';

import axios from 'axios';

import { 
  Container, 
  ChatbotContainer,
  BubbleAuthor,
  Bubble,
  TextAuthor,
  TextMessage,
  InformationModal,
  InputButtonContainer,
  ListHeroesModal,
  FormContainer,
  LoadImage,
  Form  
} from './styles';

// import Modal from '../../components/Modal'
import Modal from 'react-responsive-modal';

export default class Main extends Component {

  state = {

    newMessages: [],
    message: '',
    messageWatson: '',
    author: '',
    modalVisible: true,
    loading: false,
    username: '',
    usernameInput: false,
    error: false,
    messages: []

  };

  hideModal = () => {

    if(!this.state.username) {

      this.setState({ usernameInput: true })

      return null

    }

    localStorage.setItem('@AplicativoChatBotWatson:username', this.state.username);

    const username = localStorage.getItem('@AplicativoChatBotWatson:username');

    this.setState({ author: username })

    this.setState({ modalVisible: false })
  };

  handleAddMessage = async (e) => {

    e.preventDefault()

    let allMessages = []
  
    this.setState({ loading: true })

    await axios.post('https://heroes-chatbot.herokuapp.com/conversation', {
        text: this.state.message,
        context: {}
      })
      .then((response) => {
        this.setState({ messageWatson: response.data.output.text })
  
        const WatsonMessage = {
          id: response.data.context.conversation_id,
          text: this.state.messageWatson,
          createdAt: new Date(),
          user: {
            name: 'Nicky Fury',
            avatar: 'https://placeimg.com/140/140/any',
          },
        }
    
        const UserMessage = {
          id: response.data.context.conversation_id + 'user',
          text: this.state.message,
          createdAt: new Date(),
          user: {
            name: this.state.author,
            avatar: 'https://placeimg.com/140/140/any',
          },
        }
  
        this.setState({ newMessages: [ UserMessage, WatsonMessage ] })
  
        allMessages = [...this.state.messages, ...this.state.newMessages]
    
        this.setState({ newMessages: [], message: '', messageWatson: '', loading: false })
    
        this.setState({ messages: allMessages })
  
      })
      .catch((error) => {

        this.setState({ loading: false })

        alert('ERRO ' + JSON.stringify(error));
        
    });
  
  }

  render() {
    return (
      
    <Container>

      <div>
        <Modal open={this.state.modalVisible} onClose={this.hideModal} 
        closeOnOverlayClick={false} 
        showCloseIcon={false}
        closeOnEsc={false} center>
          <h2>Heroes Chatbot</h2>
          <InformationModal>
            <h5>
              Esse Chatbot é a personificação de Nicky Fury, agente da Shield, e pode
              te apresentar os Vingadores, os melhores heróis da Marvel
            </h5>
            <ListHeroesModal>
              <h5>Homem de Ferro</h5>
              <h5>Thor</h5>
              <h5>Capitão América</h5>
              <h5>Hulk</h5>
              <h5>Viúva Negra</h5>
              <h5>Gavião Arqueiro</h5>
            </ListHeroesModal>
            <h4>Para começar digite seu nome</h4>
            <InputButtonContainer>
            <input 
              type="text" 
              placeholder="Nome" 
              value={this.state.username}
              onChange={e => this.setState({ username: e.target.value })}
            />
            {this.state.usernameInput ? <h6>Digite seu nome</h6> : null }
            <button onClick={this.hideModal}><span>Enviar</span></button>
            </InputButtonContainer>
          </InformationModal>
        </Modal>
      </div>

      <React.Fragment>

        <ChatbotContainer>

             {this.state.messages.map(message => {

              return message.user.name === this.state.author ?

              <BubbleAuthor
                  key={message.id}
              >
              
                <TextAuthor>{message.user.name}</TextAuthor>
                <TextMessage>{message.text}</TextMessage>
              
              </BubbleAuthor> :

              <Bubble
                  key={message.id}
              >
              
                <TextAuthor>{message.user.name}</TextAuthor>
                <TextMessage>{message.text}</TextMessage>
              
              </Bubble>
  
              })
            }

            <FormContainer>

            <Form withError={this.state.error} onSubmit={this.handleAddMessage}>

            <input 
              type="text" 
              placeholder="Mensagem" 
              value={this.state.message}
              onChange={e => this.setState({ message: e.target.value })}
    
            />

            <button type="submit">{ this.state.loading ? <i className="fa fa-spinner fa-pulse" /> : "Enviar" }</button>

          </Form>  

          </FormContainer>

        </ChatbotContainer>

        </React.Fragment>

      </Container>

    );
  }
}