# heroes-chatbot-watson

## O que é ?

Esse Chatbot é a personificação de Nicky Fury, agente da Shield, e pode te apresentar os Vingadores, os melhores heróis do universo Marvel, ele tem o objetivo de demonstrar o uso da tecnologia Watson Assistant da IBM e conceitos como Intents, Dialog, Context e Entities.

Atualmente o Chatbot está treinado, para te responder perguntar sobre os seguintes heróis:

- Homem de Ferro
- Thor
- Capitão América
- Hulk
- Viúva Negra
- Gavião Arqueiro

A sua versão feita em React, está disponível para acesso nesse link https://heroes-chatbot-watson-3clm0jt2a.now.sh

A sua API que foi feita utilizando Node.js, e que integra a aplicações feitas em React Native e React, está disponível nesse link: https://heroes-chatbot.herokuapp.com

Utilizando sua essa rota da API https://heroes-chatbot.herokuapp.com/conversation, com comando POST, é possível enviar mensagens para Chatbot com o valor ```"message": "oi"``` no body da requisição, que deve ser em JSON.

![](assets/PrintAPI.png?raw=true)

### Aplicativo

O APK do Aplicativo também está disponível para download nesse [link](https://drive.google.com/open?id=1aJ-2D-0_Xp9bg44mAz6YLgCNe1GVRI6W)

**Também gravei um video mostrando o aplicativo está disponível nesse [link](https://drive.google.com/open?id=1wFPBXmelkc12NYKyjOLTgaJp2zLEjG7N)**

### Como configurar o ambiente para rodar as soluções ?

Primeiramente, é interessante que você tenha o Node.js instalado em seu computador a partir da versão 8, você pode fazer o download dele nesse [link](https://nodejs.org/en/)

- React

Agora para você rodar a aplicação React, vai ser necessário você estar na pasta ```web``` e após isso rodar o comando ```npm i```que vai ser responsável por fazer o donwload de todas as dependências necessárias, para o projeto.

Depois disso é só utilizar o comando ```npm start``` e provavelmente a aplicação já estará rodando

- Node.js

Para você rodar o servidor, é necessário, você estar na pasta ```server``` e após isso rodar também o comando ```npm i```para fazer o download, das dependências do projeto.

Depois disso é só utilizar o comando ```npm start``` e provavelmente a aplicação já estará rodando

**Caso você desejar rodar a aplicação React e o servidor em Node, ao mesmo tempo primeiro rode o Node na porta localhost:3000 como padrão com o npm start e após isso rode a aplicação em React que vai rodar na porta localhost:3001, para evitar conflito entre portas**

**E não esqueça de mudar a URL nos projetos, pois eles estão com a URL da API online**

- React Native

Para você rodar o aplicativo é necessário você estar na pasta ```app``` e após isso rodar o comando ```npm i```para fazer o download, das dependências do projeto. Agora será necessário você configurar o ambiente de desenvolvimento do React Native para Android ou IOS ou caso você quiser, as duas plataformas para fazer isso é só seguir esse [tutorial](https://rocketseat.com.br/assets/files/ambiente-de-desenvolvimento-rn.pdf) depois de você terminar a configuração, é só rodar ```react-native run-android```para rodar o aplicativo no Android ou ```react-native run-ios```para rodar o aplicativo no IOS

Também lembrando caso você estiver rodando a API localmente, também será necessário trocar a URL

**Como você decidiu pelas tecnologias que usou no projeto ?**

Além de utilizar a última versão do Watson Assistant no projeto, utilizei as seguintes tecnologias:

Nas três soluções que foram desenvolvidas, utilizei o Javascript uma das linguagens de programação, mais usadas no momento, justamente por causa da sua versatilidade, e quantidade de bibliotecas e frameworks disponíveis no mercado.

Para o Front-End da aplicação utilizei o **React** uma biblioteca para o desenvolvimento de interfaces web, criada pelo Facebook, open source com uma comunidade de milhares de desenvolvedores, que conta com muitos recursos interessantes, como ser baseada em componentes, facilitando o desenvolvimento de diversas formas.

Para o aplicativo Mobile utilizei o **React Native**, um framework feito com base no React para o desenvolvimento de aplicativos para Android e IOS totalmente nativos com código Javascript, é utilizado por grandes empresas como Uber, Netflix, Nubank entre outras, e também acho que é utilizado em alguns projetos aí na IBM. Confesso que essa é a tecnologia que mais gosto de utilizar no momento.

Já para o Back-End utilizei o **Node**, uma ferramenta que nos permite rodar Javascript em server-side, escolhi ele pois graças as suas features como o Event-Loop é uma das ferramentas, mais escaláveis no mercado, e também já é utilizado por muitas empresas, além de também contar com uma comunidade gigante.

**Existe alguma melhoria que você gostaria de fazer no seu projeto após tê-lo enviado?**

Acredito que meu projeto ainda precisa de algumas melhorias, principalmente no Chatbot, acredito que caso tivesse mais tempo, poderia ter treinado ele, para saber mais conteúdo sobre esses heróis, e também adicionado mais heróis, além de trabalhar mais com o contexto do Chatbot em algumas partes do diálogo, e fornecer mais personalidade e indentidade para o Chatbot.

Além disso gostaria de ter trabalho melhor no design da página feita em React, e também na distruição de informação e em algumas páginas do aplicativo feito utilizando React Native

**O que você faria diferente se tivesse mais tempo disponível?**

Com mais tempo disponível, além das alterações que disse na outra pergunta, eu iria trabalhar com outros recursos do catálogo, da IBM Cloud que podem ser muitos úteis para um Chatbot como o Tone Analyzer, para saber como os usuários estão interagindo, com o Chatbot, o Visual Recognition com seu uso seria possível enviar imagens de heróis para o Chatbot, e ele identificaria qual herói seria pela foto ou imagem. Além disso para ampliar o uso do Chatbot, seria legal utilizar os recursos de Text To Speech e Speech To Text.

Também gostaria de fazer integrações com o Chatbot para alguns serviços como o Facebook Messenger, alcançando mais usuários.

### Algumas Imagens de como o Chatbot está configurado na IBM Cloud



![](assets/PrintIntent.png?raw=true)


Essa imagem está mostrando alguns dos Intents do Chatbot


![](assets/PrintEntities.png?raw=true)


Essa imagem está mostrando alguns dos Entities do Chatbot


![](assets/PrintDialog.png?raw=true)


Essa imagem está mostrando alguns das nodes do Chatbot


![](assets/PrintDialogI.png?raw=true)


Essa imagem está mostrando alguns das nodes do Chatbot


![](assets/PrintDialog1.png?raw=true)


Essa imagem está mostrando alguns das nodes do Chatbot


Qualquer dúvida ou precisar de alguma coisa, é só chamar
