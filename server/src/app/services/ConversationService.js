
// const fs = require("fs");
const AssistantV1 = require('watson-developer-cloud/assistant/v1');

module.exports = {

    async userMessage(req, res, next) {

        const { text, context = {} } = req.body;

        if(text === null || text === '') {

            res.status(404);
            res.json('Digite uma mensagem');

            return;
            
        }

        const assistant = new AssistantV1({
            username: 'apikey',
            password: 'vwIyLRoZmXZ9--ypDAEyPnVlznmM2JuPN-lEpwDeFVjQ',
            url: 'https://gateway.watsonplatform.net/assistant/api/',
            version: '2019-03-15',
            disable_ssl_verification: true,
        });

        const params = {
            input: { text: text },
            workspace_id: '91b1091b-db37-48fb-b601-557f490831f3',
            context
        }

        assistant.message(params, (err, response) => {

            if (err) {
                console.error(err);
                res.status(500).json(err);
            } else {
                console.log(JSON.stringify('A seguinte mensagem foi enviada : ' + text, null, 2));
                res.json(response);
            }

        });
   
    },

};