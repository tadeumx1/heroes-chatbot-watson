import { createBottomTabNavigator, createStackNavigator, createSwitchNavigator, createAppContainer } from "react-navigation";

import Heroes from "./pages/Heroes/Heroes";
import Chatbot from "./pages/Chatbot/Chatbot";
import Information from "./pages/Information/Information";

import Icon from "react-native-vector-icons/Feather";

import LoginScreen from "./pages/Welcome/Welcome";
// import RegisterUser from "./pages/RegisterUser/RegisterUser";
import AuthLoadingScreen from "./pages/AuthLoadingScreen";
// import HomeLoadingScreen from "./pages/HomeLoadingScreen";
import React from "react";

const TabNav = createBottomTabNavigator(
	{
		Information: {
			screen: Information,
			navigationOptions: {
				title: 'Sobre'
			}
		},
		Chatbot: {
			screen: Chatbot,
			navigationOptions: {
				title: 'Chatbot'
			}
		},
		/* Routes: {
			screen: Routes,
			navigationOptions: {
				title: 'Rotas'
			}
		}, */
		Heroes: {
			screen: Heroes,
			navigationOptions: {
				title: 'Heroes'
			}
		},
	},
	{
		defaultNavigationOptions: ({ navigation }) => ({
			tabBarIcon: ({ focused, tintColor }) => {
				const { routeName } = navigation.state;
				let iconName;

				switch (routeName) {
					case 'Information':
						iconName = `info`;
						break;
					case 'Chatbot':
						iconName = `message-circle`;
						break;
					/* case 'Routes':
						iconName = `map-pin`;
						break; */
					case 'Heroes':
						iconName = `user`;
						break;	
				}

				// You can return any component that you like here! We usually use an
				// icon component from react-native-vector-icons
				return <Icon name={iconName} size={24} color={tintColor} />;
			},
		}),
		initialRouteName: 'Chatbot',
		tabBarOptions: {
			activeTintColor: '#7A91CA',
			inactiveTintColor: '#000',
			style: {
				backgroundColor: '#FFFFFF',
				// height: metrics.tabBarHeight,
				paddingHorizontal: 20,
				borderColor: '#eee',
			}
		},
	}
);

const TabNavContainer = createAppContainer(TabNav);

const AuthStack = createStackNavigator(
	{
		SignIn: LoginScreen,
		App: TabNavContainer,
		// SignUpStudent: RegisterUser
	},
	{
		initialRouteName: 'SignIn',
		headerMode: 'none',
		header: null
	}
);

const AuthStackContainer = createAppContainer(AuthStack);

const RootStack = createSwitchNavigator(
	{
		AuthLoading: { screen: AuthLoadingScreen },
		App: { screen: TabNavContainer },
		Auth: { screen: AuthStackContainer }
	},
	{
		initialRouteName: 'AuthLoading',
		headerMode: 'none',
		navigationOptions: {
			header: null
		}
	}
);

const RootStackContainer = createAppContainer(RootStack);

export default RootStackContainer
