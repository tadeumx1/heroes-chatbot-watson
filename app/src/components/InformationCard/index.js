import React from "react";
import { StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import PropTypes from "prop-types";
import { Container, Title, InfoContainer, Info, InfoText } from "./styles";

const InformationCard = ({ title, iconName, infoText }) => {
  return (
    <Container>
      <Title>{title}</Title>

      <InfoContainer>
        <Info>
          <InfoText>{infoText}</InfoText>
        </Info>
      </InfoContainer>
    </Container>
  );
};

const styles = StyleSheet.create({

  infoIcon: {

    color: "#666"
    
  }

});

export default InformationCard;
