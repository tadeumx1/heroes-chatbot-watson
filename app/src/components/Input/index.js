import React, { Component } from 'react';

import { 
    View,
    Text,
    TextInput, 
    StyleSheet,
    AsyncStorage,
    ActivityIndicator,
    TouchableOpacity 
} from 'react-native';

import axios from 'axios';

export default class Input extends Component {

  state = {

    newMessages: [],
    message: '',
    messageWatson: '',
    author: '',
    loading: false,

  };

  async componentDidMount() {

    const username = await AsyncStorage.getItem('@AplicativoChatBotWatson:username');
    this.setState({ author: username })

  }

  handleAddMessage = async () => {

    let allMessages = []

    this.setState({ loading: true })

    await axios.post('https://heroes-chatbot.herokuapp.com/conversation', {
        text: this.state.message,
        context: {}
      })
      .then((response) => {
        this.setState({ messageWatson: response.data.output.text })

        const WatsonMessage = {
          id: response.data.context.conversation_id,
          text: this.state.messageWatson,
          createdAt: new Date(),
          user: {
            name: 'Nicky Fury',
            avatar: 'https://placeimg.com/140/140/any',
          },
        }
    
        const UserMessage = {
          id: response.data.context.conversation_id + 'user',
          text: this.state.message,
          createdAt: new Date(),
          user: {
            name: this.state.author,
            avatar: 'https://placeimg.com/140/140/any',
          },
        }

        this.setState({ newMessages: [ UserMessage, WatsonMessage ] })

        allMessages = [...this.props.messages, ...this.state.newMessages]
    
        this.setState({ newMessages: [], message: '', messageWatson: '', loading: false })
    
        this.props.newMessages(allMessages)

      })
      .catch((error) => {

        this.setState({ loading: false })

        alert('ERRO ' + JSON.stringify(error));

    });

  }

  render() {
    return (

      <View style={styles.inputContainer}>

        <TextInput
            style={styles.input}
            underlineColorAndroid="rgba(0, 0, 0, 0)"
            value={this.state.message}
            onChangeText={message => this.setState ({ message })}

        />

        <TouchableOpacity onPress={this.handleAddMessage}>

          {this.state.loading ?  
          ( 
            <ActivityIndicator size="small" color="#00ff00" />
          ) : (
            <Text style={styles.button}>Enviar</Text>
          )

          }
        
        </TouchableOpacity>

      </View>
      
    );
  }
}

const styles = StyleSheet.create ({

    inputContainer: {

      height: 42,
      paddingHorizontal: 10,
      paddingVertical: 6,
      backgroundColor: '#fafafa',
      borderTopWidth: StyleSheet.hairlineWidth,
      borderTopColor: '#CCC',
      flexDirection: 'row',
      alignItems: 'center',

    },

    input: {

      flex: 1,
      height: 30,
      paddingHorizontal: 10,
      paddingVertical: 0,
      backgroundColor: '#FFF',
      borderWidth: 1,
      borderColor: '#ddd',
      borderRadius: 12,

    },

    button: {

      marginLeft: 10,
      color: '#358CFF',
      fontWeight: 'bold',
      
    }

});