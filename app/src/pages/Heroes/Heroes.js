import React, { Component } from 'react'
import { View, Text, TouchableOpacity, ImageBackground, StyleSheet } from 'react-native'

import Swiper from 'react-native-swiper'

export default class Heroes extends Component {

  handleButtonMenu = () => {
  }

  render() {
    return (
      <View style={styles.container}>
        <Swiper style={styles.wrapper} showsButtons>
          <View style={styles.slide1}>
          <ImageBackground
            source={{ uri:'https://wallpapercave.com/wp/wp3003497.jpg' }}
            style={{width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center' }}
          > 
            <Text style={styles.text}>Homem de Ferro</Text>
            </ImageBackground>
          </View>
          <View style={styles.slide2}>
          <ImageBackground
            source={{ uri:'https://img.elo7.com.br/product/zoom/11E8F95/painel-thor-g-frete-gratis-fundo-festa-infantil.jpg' }}
            style={{width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center' }}
          > 
            <Text style={styles.text}>Thor</Text>
            </ImageBackground>
          </View>
          <View style={styles.slide3}>
          <ImageBackground
            source={{ uri:'https://wallpapercave.com/wp/wp1808975.jpg' }}
            style={{width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center' }}
          > 
            <Text style={styles.text}>Capitão América</Text>
            </ImageBackground>
          </View>
          <View style={styles.slide3}>
          <ImageBackground
            source={{ uri:'https://wallpapers.moviemania.io/phone/movie/1724/a4a1d8/the-incredible-hulk-phone-wallpaper.jpg?w=1536&h=2732' }}
            style={{width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center' }}
          > 
            <Text style={styles.text}>Hulk</Text>
            </ImageBackground>
          </View>
          <View style={styles.slide3}>
          <ImageBackground
            source={{ uri:'https://i.pinimg.com/736x/6b/76/0e/6b760e82d429cc4899d9c9d881c3b250.jpg' }}
            style={{width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center' }}
          > 
            <Text style={styles.text}>Viúva Negra</Text>
            </ImageBackground>
          </View>
          <View style={styles.slide3}>
          <ImageBackground
            source={{ uri:'https://i.kinja-img.com/gawker-media/image/upload/s--qFwf2Dk4--/c_scale,f_auto,fl_progressive,q_80,w_800/e4u1njnr4shj3qoz4ncy.jpg' }}
            style={{width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center' }}
          > 
            <Text style={styles.text}>Gavião Arqueiro</Text>
          </ImageBackground>
          </View>
          </Swiper>
      </View>
    )
  }
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
  },

  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB'
  },

  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#97CAE5'
  },

  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9'
  },
  
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold'
  }

})

Heroes.propTypes = {



}

