import React from 'react';
import { ScrollView, ImageBackground, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { Container, Loading } from './styles'
import InformationCard from '../../components/InformationCard';

const text = `Esse Chatbot é a personificação de Nicky Fury, agente da Shield, e pode te apresentar os Vingadores, os melhores heróis 
do universo Marvel, ele tem o objetivo de demonstrar o uso da tecnologia Watson Assistant da IBM e conceitos como Intents, Dialog, Context e Entities.\n 
Atualmente o Chatbot está treinado, para te responder perguntar sobre os seguintes heróis:

- Homem de Ferro\n
- Thor\n
- Capitão América\n
- Hulk\n
- Viúva Negra\n
- Gavião Arqueiro`;

const Information = (props) => {

    const remote = 'https://wallpaperplay.com/walls/full/e/0/8/119623.jpg';

    return (

        <ImageBackground
        style={styles.image}
        source={{ uri: remote }}
        >

            <Container>
                <ScrollView>

                  <InformationCard title={'Sobre o Aplicativo'} iconName={'list'} infoText={text} />

                </ScrollView>
            </Container>

        </ImageBackground>

    )
};

const styles = StyleSheet.create({

    image: {
        backgroundColor: '#ccc',
        flex: 1,
        position: 'absolute',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
    },

})

Information.propTypes = {



}

export default Information
