import React, { Component } from 'react'
import { 
  View, 
  Text, 
  TouchableOpacity, 
  Dimensions, 
  Platform, 
  AsyncStorage,
  ScrollView,
  KeyboardAvoidingView, 
  ImageBackground,
  StyleSheet, 
  StatusBar 
} from 'react-native'

import Input from '../../components/Input';

StatusBar.setBarStyle('light-content');

const remote = 'https://wallpaperplay.com/walls/full/e/0/8/119623.jpg';

export default class Chatbot extends Component {

  state = {

    messages: [],
    author: null

  }

  async componentDidMount() {

    const username = await AsyncStorage.getItem('@AplicativoChatBotWatson:username');
    this.setState({ author: username })
    
  }

  handleNewMessages = (newMessages) => {
    this.setState({ messages: newMessages })
  }

  render() {
    return (

      <KeyboardAvoidingView 
      style={styles.container}
      behavior={Platform.OS === 'ios' ? 'padding' : null}

      >

      <ImageBackground
        style={styles.image}
        source={{ uri: remote }}
      >

          <ScrollView 
              contentContainerStyle={styles.conversation}
              keyboardShouldPersistTaps="never"
              ref={scrollView => this._scrollView = scrollView}
              onContentSizeChange={(width, height) => this._scrollView.scrollToEnd({ animated: true })}
              >
               
             {this.state.messages.map(message => (

              <View
                  key={message.id}
                  style={[
  
                      styles.bubble,
                      message.user.name === this.state.author
                       ? styles ['bubble-right']
                       : styles ['bubble-left']
  
                  ]}
              >
              
                  <Text style={styles.author}>{message.user.name}</Text>
                  <Text style={styles.message}>{message.text}</Text>
              
              </View>
  
              ))
            }
               
          </ScrollView>
  
          <Input messages={this.state.messages} newMessages={this.handleNewMessages} />

      </ImageBackground>
  
      </KeyboardAvoidingView>

    )
  }
}

const { width } = Dimensions.get('window');

const styles = StyleSheet.create ({

    container: {

        flex: 1,
        backgroundColor: '#2C4241',
        ...Platform.select({

            ios: { paddingTop: 20 },

        }),

    },

    conversation: {

      padding: 10,

    },

    loading: {

      marginTop: 20,

    },

    image: {
      backgroundColor: '#ccc',
      flex: 1,
      position: 'absolute',
      width: '100%',
      height: '100%',
      justifyContent: 'center',
    },

    bubble: {

      padding: 6,
      backgroundColor: '#F5F5F5',
      borderRadius: 6,
      shadowColor: 'rgba(0, 0, 0, 0.5)',
      shadowOffset: {

        width: 0,
        height: 1,

      },

      shadowOpacity: 0.5,
      shadowRadius: 0,
      marginTop: 10,
      maxWidth: width - 60,

    },

    'bubble-left': {

      alignSelf: 'flex-start',

    },

    'bubble-right': {

      alignSelf: 'flex-end',
      backgroundColor: '#D1EDC1'

    },

    author: {

      fontWeight: 'bold',
      marginBottom: 3,
      color: '#333'

    },

    message: {

      fontSize: 16,
      color: '#333',

    }

});

Chatbot.propTypes = {



}

